# ranking

![NestJS](https://img.shields.io/badge/v10.0.0-grey?logo=nestjs&label=nestjs&labelColor=red&style=for-the-badge)
![NodeJS](https://img.shields.io/badge/v20.9.0-grey?logo=nodedotjs&logoColor=white&label=node&labelColor=green&style=for-the-badge)
![Postgresql](https://img.shields.io/badge/v15.4-grey?logo=postgresql&logoColor=white&label=postgresql&labelColor=blue&style=for-the-badge)
![Docker](https://img.shields.io/badge/docker-blue?logo=docker&logoColor=white&style=for-the-badge)

A REST API to manage **players** and their **scores**.

## Authors

[![gklav](https://img.shields.io/badge/gitlab-blue?logo=gitlab&label=gklav&style=for-the-badge)](https://gitlab.com/gklav)

## Summary

- [Overview](#Overview)
- [Quick start](#Quick-start)
- [.env](#.env)
- [Routes](#Routes)

## Overview

A **player** has a **score**. Players are ordered by **rank** depending on their scores.

### Functionalities

- Create a new **player**
- Update a **player**'s score
- Fatch details about a specific player
- Fetch the list of all **players** ordered by **rank**
- Remove all players

**Player**

- name (_string_): name/nickname of a player.
- score: (_number_): amount of points of a player.
- rank: (_number_): rank of a player. Calculated by the API.

## Quick start

### Dependencies

- make
- Docker
- docker-compose

### Run Locally

Clone the project

```bash
  git clone https://gitlab.com/gklav/familyties
```

Go to the project directory

```bash
  cd my-project
```

Add .env file (see [.env](##.env) section)\
Run the application

```bash
  make start
```

#### Other commands

_Stop all containers_

```bash
  make stop
```

_Build new images and run the application_

```bash
  make build
```

_Remove all unused containers, images, volumes and networks_

```bash
  make clean
```

## .env

Environment variables <br/>
To run this project, you will need to add the following environment variables to your .env file

`POSTGRES_HOST`: string

`POSTGRES_PORT`: number

`POSTGRES_DATABASE`: number

`POSTGRES_USER`: string

`POSTGRES_PASSWORD`: string

`POSTGRES_AUTOLOADENTITIES`: boolean

## Routes

| Route          | Action | Description                       |
| -------------- | ------ | --------------------------------- |
| /api           | GET    | Swagger (OpenApi) documentation   |
| /players       | GET    | Fetch all players ordered by rank |
| /players       | POST   | Create a new player               |
| /players       | DELETE | Romove all players                |
| /players/:name | GET    | Fetch one specific player         |
| /players/:name | PATCH  | Update a player's score           |
