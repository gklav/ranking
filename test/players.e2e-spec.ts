import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('Players (e2e)', () => {
  let app: INestApplication;

  const createPlayer: any = {
    name: 'johndoe',
  };

  const mockPlayer: any = {
    ...createPlayer,
    score: 0,
    rank: 1,
  };

  const createSecondPlayer: any = {
    name: 'janedoe',
  };

  const mockSecondPlayer: any = {
    ...createSecondPlayer,
    score: 0,
    rank: 2,
  };

  const updatePlayer: any = {
    score: 40,
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/players (POST)', () => {
    it('Fail to create a player - Wrong type for authorized field - 400', () => {
      return request(app.getHttpServer())
        .post('/players')
        .send({ name: 40 })
        .expect(400);
    });

    it('Create a new player - 200', () => {
      return request(app.getHttpServer())
        .post('/players')
        .send(createPlayer)
        .expect(201)
        .then(({ body }) => {
          expect(body).toEqual(mockPlayer);
        });
    });

    it('Create a second player - 200', () => {
      return request(app.getHttpServer())
        .post('/players')
        .send(createSecondPlayer)
        .expect(201)
        .then(({ body }) => {
          expect(body).toEqual(mockSecondPlayer);
        });
    });

    it('Fail to create a player - Name already taken - 409', () => {
      return request(app.getHttpServer())
        .post('/players')
        .send(createPlayer)
        .expect(409);
    });
  });

  describe('/players (GET)', () => {
    it('Fetch all players - 200', () => {
      return request(app.getHttpServer()).get('/players').expect(200);
    });
  });

  describe('/players/:name (GET)', () => {
    it('Fail to fetch a player - Player not found - 404', () => {
      return request(app.getHttpServer())
        .get('/players/' + 'radomValue')
        .expect(404);
    });

    it('Fetch a player - 200', () => {
      return request(app.getHttpServer())
        .get('/players/' + createPlayer.name)
        .expect(200)
        .then(({ body }) => {
          expect(body).toEqual(mockPlayer);
        });
    });
  });

  describe('/players/:name (PATCH)', () => {
    it('FAil to update a player - PLayer not found - 404', () => {
      return request(app.getHttpServer())
        .patch('/players/' + 'randomValue')
        .send(updatePlayer)
        .expect(404);
    });

    it('Fail to update a player - Wrong type for authorized field - 400', () => {
      return request(app.getHttpServer())
        .post('/players')
        .send({ score: 'score' })
        .expect(400);
    });

    it('Update a player - 200', () => {
      return request(app.getHttpServer())
        .patch('/players/' + createSecondPlayer.name)
        .send(updatePlayer)
        .expect(200)
        .then(({ body }) => {
          expect(body).toEqual({
            ...mockSecondPlayer,
            score: updatePlayer.score,
            rank: 1,
          });
        });
    });
  });

  describe('/players (DELETE)', () => {
    it('Should delete all players - 200', () => {
      return request(app.getHttpServer()).delete('/players').expect(200);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
