DC=docker-compose

start:
	$(DC) up -d

build:
	$(DC) up -d --build --remove-orphans

stop:
	$(DC) down

clean:
	docker system prune
