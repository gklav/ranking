FROM node:20.9.0-alpine3.17 AS build

WORKDIR /app
COPY --chown=node:node ./package.json .
RUN npm install

COPY --chown=node:node . .
RUN npm run build

FROM node:20.9.0-alpine3.17 AS production

ARG NODE_ENV=production
WORKDIR /app
COPY --chown=node:node ./package.json .
RUN npm install --only-prod

COPY --chown=node:node --from=build /app/dist ./dist
COPY --chown=node:node .env ./dist

CMD npm run migration:run; npm run start:prod
