import { MigrationInterface, QueryRunner } from 'typeorm';

export class PlayerV11700484944281 implements MigrationInterface {
  name = 'PlayerV11700484944281';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "player" ("name" character varying NOT NULL, "score" integer NOT NULL, "rank" integer NOT NULL, CONSTRAINT "PK_7baa5220210c74f8db27c06f8b4" PRIMARY KEY ("name"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "player"`);
  }
}
