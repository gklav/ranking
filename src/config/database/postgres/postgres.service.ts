import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { postgresDataSourceOptions } from './typeorm.config';

@Injectable()
export class PostgresService implements TypeOrmOptionsFactory {
  constructor(private configService: ConfigService) {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      ...postgresDataSourceOptions,
      autoLoadEntities: this.configService.get<boolean>(
        'POSTGRES_AUTOLOADENTITIES',
      ),
    };
  }
}
