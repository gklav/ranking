import { Test, TestingModule } from '@nestjs/testing';
import { PlayersController } from './players.controller';
import { PlayersService } from './players.service';
import { CreatePlayerDto } from './dto/create-player.dto';
import { Player } from './player.entity';
import { UpdatePlayerDto } from './dto/update-player.dto';

describe('PlayersController', () => {
  let playersController: PlayersController;
  let playersService: PlayersService;

  const mockPlayer: Player = {
    name: 'johndoe',
    score: 0,
    rank: 30,
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlayersController],
      providers: [
        PlayersService,
        {
          provide: PlayersService,
          useValue: {
            create: jest.fn().mockResolvedValue(mockPlayer),
            findAll: jest.fn().mockResolvedValue(Array<Player>()),
            findOne: jest.fn().mockResolvedValue(mockPlayer),
            removeAll: jest.fn(),
            update: jest.fn().mockImplementation((name, dto) => {
              return Promise.resolve({
                ...mockPlayer,
                score: dto.score,
                rank: 20,
              });
            }),
          },
        },
      ],
    }).compile();

    playersController = module.get<PlayersController>(PlayersController);
    playersService = module.get<PlayersService>(PlayersService);
  });

  it('should be defined', () => {
    expect(playersController).toBeDefined();
  });

  describe('create()', () => {
    it('Should return a saved Player', () => {
      const mockCreatePlayerDto: CreatePlayerDto = {
        name: mockPlayer.name,
      };

      playersController.create(mockCreatePlayerDto);

      expect(playersController.create(mockCreatePlayerDto)).resolves.toEqual(
        mockPlayer,
      );

      expect(playersService.create).toHaveBeenCalledWith(mockCreatePlayerDto);
    });
  });

  describe('findAll()', () => {
    it('Should return a list of Players', () => {
      playersController.findAll();

      expect(playersController.findAll()).resolves.toMatchObject(
        new Array<Player>(),
      );

      expect(playersService.findAll).toHaveBeenCalled();
    });
  });

  describe('removeAll()', () => {
    it('Should return delete all Players', () => {
      playersController.removeAll();

      expect(playersService.removeAll).toHaveBeenCalled();
    });
  });

  describe('find()', () => {
    it('Should return details of a Player', () => {
      playersController.findOne(mockPlayer.name);

      expect(playersController.findOne(mockPlayer.name)).resolves.toEqual(
        mockPlayer,
      );

      expect(playersService.findOne).toHaveBeenCalledWith(mockPlayer.name);
    });
  });

  describe('update()', () => {
    it('Should return an updated Player', () => {
      const mockUpdatePlayerDto: UpdatePlayerDto = {
        score: 20,
      };

      playersController.update(mockPlayer.name, mockUpdatePlayerDto);

      expect(
        playersController.update(mockPlayer.name, mockUpdatePlayerDto),
      ).resolves.toEqual({
        ...mockPlayer,
        score: 20,
        rank: 20,
      });

      expect(playersService.update).toHaveBeenCalledWith(
        mockPlayer.name,
        mockUpdatePlayerDto,
      );
    });
  });
});
