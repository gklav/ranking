import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePlayerDto } from './dto/create-player.dto';
import { UpdatePlayerDto } from './dto/update-player.dto';
import { Player } from './player.entity';

@Injectable()
export class PlayersService {
  constructor(
    @InjectRepository(Player)
    private playersRepository: Repository<Player>,
  ) {}

  async findOnePlayerByName(name: string): Promise<Player> {
    try {
      return await this.playersRepository.findOneBy({ name: name });
    } catch (err) {
      throw err;
    }
  }

  async findRank(newScore: number, previousScore?: number) {
    previousScore = previousScore ? previousScore : -Infinity;

    try {
      return (
        (await this.playersRepository
          .createQueryBuilder('player')
          .where('player.score >= :newScore', {
            newScore: newScore,
          })
          .getCount()) + (newScore > previousScore ? 1 : 0)
      );
    } catch (err) {
      throw err;
    }
  }

  async create(createPlayerDto: CreatePlayerDto): Promise<Player> {
    const defaultScore = 0;

    if (await this.findOnePlayerByName(createPlayerDto.name)) {
      throw new ConflictException('Name already taken');
    }

    const initialRank = await this.findRank(defaultScore);

    const createPlayer: Player = {
      ...createPlayerDto,
      score: defaultScore,
      rank: initialRank,
    };

    try {
      return await this.playersRepository.save(createPlayer);
    } catch (err: any) {
      throw err;
    }
  }

  async findAll(): Promise<Array<Player>> {
    try {
      return await this.playersRepository.find({ order: { rank: 'ASC' } });
    } catch (err) {
      throw err;
    }
  }

  async removeAll(): Promise<string> {
    try {
      await this.playersRepository.delete({});
      return 'Player table is empty';
    } catch (err) {
      throw err;
    }
  }

  async findOne(name: string): Promise<Player> {
    const player = await this.findOnePlayerByName(name);

    if (!player) {
      throw new NotFoundException();
    }

    return player;
  }

  async update(
    name: string,
    updatePlayerDto: UpdatePlayerDto,
  ): Promise<Player> {
    const updatePlayer = await this.findOne(name);
    const newScore = updatePlayerDto.score;
    const previousScore = updatePlayer.score;
    const previousRank = updatePlayer.rank;

    if (updatePlayer.score != newScore) {
      updatePlayer.score = updatePlayerDto.score;
      const newRank = await this.findRank(newScore, previousScore);

      if (newRank < updatePlayer.rank) {
        await this.playersRepository
          .createQueryBuilder('players')
          .update(Player)
          .set({
            rank: () => 'rank + 1',
          })
          .where('rank >= :newRank', { newRank: newRank })
          .andWhere('rank < :previousRank', { previousRank: previousRank })
          .execute();
      }

      if (newRank > updatePlayer.rank) {
        await this.playersRepository
          .createQueryBuilder('players')
          .update(Player)
          .set({
            rank: () => 'rank - 1',
          })
          .where('rank <= :newRank', { newRank: newRank })
          .andWhere('rank > :previousRank', {
            previousRank: previousRank,
          })
          .execute();
      }

      updatePlayer.rank = newRank;
      await this.playersRepository.update(updatePlayer.name, updatePlayer);
    }

    return updatePlayer;
  }
}
