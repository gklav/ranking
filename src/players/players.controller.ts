import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  ValidationPipe,
} from '@nestjs/common';
import { CreatePlayerDto } from './dto/create-player.dto';
import { Player } from './player.entity';
import { PlayersService } from './players.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { UpdatePlayerDto } from './dto/update-player.dto';

@Controller('players')
export class PlayersController {
  constructor(private playersService: PlayersService) {}

  @Post()
  @ApiTags('players')
  @ApiResponse({
    status: 201,
    description: 'The player has been successfully created.',
    type: Player,
  })
  @ApiResponse({ status: 400, description: 'Invalid request body.' })
  @ApiResponse({ status: 409, description: "Player's name is already taken." })
  async create(
    @Body(ValidationPipe) createPlayerDto: CreatePlayerDto,
  ): Promise<Player> {
    return await this.playersService.create(createPlayerDto);
  }

  @Get()
  @ApiTags('players')
  @ApiResponse({
    status: 200,
    description: 'List of all the players.',
    type: [Player],
  })
  async findAll(): Promise<Array<Player>> {
    return await this.playersService.findAll();
  }

  @Delete()
  @ApiTags('players')
  @ApiResponse({
    status: 200,
    description: 'Player table is empty.',
  })
  async removeAll(): Promise<string> {
    return this.playersService.removeAll();
  }

  @Get(':name')
  @ApiTags('players')
  @ApiResponse({
    status: 200,
    description: 'Details of a player',
    type: Player,
  })
  @ApiResponse({
    status: 404,
    description: 'Player not found',
  })
  async findOne(@Param('name') name: string): Promise<Player> {
    return await this.playersService.findOne(name);
  }

  @Patch(':name')
  @ApiTags('players')
  @ApiResponse({
    status: 200,
    description: 'Updated player',
    type: Player,
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid request',
  })
  @ApiResponse({
    status: 404,
    description: 'Player not found',
  })
  async update(
    @Param('name') name: string,
    @Body(ValidationPipe) updatePlayerDto: UpdatePlayerDto,
  ): Promise<Player> {
    return await this.playersService.update(name, updatePlayerDto);
  }
}
