import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PlayersService } from './players.service';
import { Player } from './player.entity';
import { CreatePlayerDto } from './dto/create-player.dto';
import { UpdatePlayerDto } from './dto/update-player.dto';

describe('PlayersService', () => {
  let playersService: PlayersService;
  let playersRepository: Repository<Player>;

  const mockPlayer: Player = {
    name: 'johndoe',
    score: 5,
    rank: 30,
  };

  const mockCreatePlayer: Player = {
    name: 'janeDoe',
    score: 10,
    rank: 20,
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PlayersService,
        {
          provide: getRepositoryToken(Player),
          useValue: {
            save: jest.fn().mockResolvedValue(mockCreatePlayer),
            findOneBy: jest
              .fn()
              .mockImplementation((where: { name: string }) => {
                return Promise.resolve(
                  where.name === mockPlayer.name ? mockPlayer : null,
                );
              }),
            createQueryBuilder: jest.fn(() => ({
              where: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              getCount: jest.fn().mockResolvedValue(mockPlayer.rank - 1),
              update: jest.fn().mockReturnThis(),
              set: jest.fn().mockReturnThis(),
              execute: jest.fn().mockReturnThis(),
            })),
            find: jest.fn().mockResolvedValue(Array<Player>()),
            delete: jest.fn(),
            update: jest.fn().mockImplementation((obj: any) => {
              Promise.resolve(obj);
            }),
          },
        },
      ],
    }).compile();

    playersService = module.get<PlayersService>(PlayersService);
    playersRepository = module.get<Repository<Player>>(
      getRepositoryToken(Player),
    );
  });

  it('should be defined', () => {
    expect(playersService).toBeDefined();
  });

  describe('findOnePlayerByName()', () => {
    it('Should return a single Player', () => {
      playersService.findOnePlayerByName(mockPlayer.name);

      expect(
        playersService.findOnePlayerByName(mockPlayer.name),
      ).resolves.toEqual(mockPlayer);
    });
  });

  describe('findRank()', () => {
    it('Should return the rank of a Player based on its score', () => {
      playersService.findRank(mockPlayer.score);

      expect(playersService.findRank(mockPlayer.score)).resolves.toEqual(
        mockPlayer.rank,
      );
    });

    it('Should return the rank of a Player based on its previous and new score (new > previous)', () => {
      playersService.findRank(mockPlayer.score);

      expect(playersService.findRank(mockPlayer.score, 0)).resolves.toEqual(
        mockPlayer.rank,
      );
    });

    it('Should return the rank of a Player based on its previous and new score (new < previous)', () => {
      playersService.findRank(mockPlayer.score);

      expect(
        playersService.findRank(mockPlayer.score, mockPlayer.score + 1),
      ).resolves.toEqual(mockPlayer.rank - 1);
    });
  });

  describe('create()', () => {
    it('Should return a saved Player', () => {
      const mockCreatePlayerDto: CreatePlayerDto = {
        name: mockCreatePlayer.name,
      };

      playersService.create(mockCreatePlayerDto);

      expect(playersService.create(mockCreatePlayerDto)).resolves.toEqual(
        mockCreatePlayer,
      );
    });
  });

  describe('findAll()', () => {
    it('Should return a list of Players', () => {
      playersService.findAll();

      expect(playersService.findAll()).resolves.toMatchObject(
        new Array<Player>(),
      );

      expect(playersRepository.find).toHaveBeenCalled();
    });
  });

  describe('removeAll()', () => {
    it('Should remove all Players rom the database', () => {
      playersService.removeAll();

      expect(playersRepository.delete).toHaveBeenCalled();
    });
  });

  describe('findOne()', () => {
    it('Should return details of a Player', () => {
      playersService.findOne(mockPlayer.name);

      expect(playersService.findOne(mockPlayer.name)).resolves.toEqual(
        mockPlayer,
      );
    });
  });

  describe('update()', () => {
    it('Should return an Updated Player. New score == previous score', () => {
      const mockUpdatePlayerDto: UpdatePlayerDto = {
        score: mockPlayer.score,
      };

      playersService.update(mockPlayer.name, mockUpdatePlayerDto);

      expect(
        playersService.update(mockPlayer.name, mockUpdatePlayerDto),
      ).resolves.toEqual(mockPlayer);
    });
  });

  it('Should return an Updated Player. New score > previous score', () => {
    const mockUpdatePlayerDto: UpdatePlayerDto = {
      score: mockPlayer.score + 20,
    };

    playersService.update(mockPlayer.name, mockUpdatePlayerDto);

    expect(
      playersService.update(mockPlayer.name, mockUpdatePlayerDto),
    ).resolves.toEqual(mockPlayer);
  });

  it('Should return an Updated Player. New score < previous score', () => {
    const mockUpdatePlayerDto: UpdatePlayerDto = {
      score: mockPlayer.score - 10,
    };

    playersService.update(mockPlayer.name, mockUpdatePlayerDto);

    expect(
      playersService.update(mockPlayer.name, mockUpdatePlayerDto),
    ).resolves.toEqual(mockPlayer);
  });
});
