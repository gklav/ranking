import { IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdatePlayerDto {
  @ApiProperty({
    description: "Player's points",
  })
  @IsNumber()
  score: number;
}
