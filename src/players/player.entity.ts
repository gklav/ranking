import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Player {
  @PrimaryColumn({ unique: true })
  @ApiProperty({
    description: 'Name of a player',
  })
  name: string;

  @Column()
  @ApiProperty({
    description: "Player's points",
  })
  score: number;

  @Column()
  @ApiProperty({
    description: "Player's rank",
  })
  rank: number;
}
